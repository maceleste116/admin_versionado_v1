package qa.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EditarVersionadoServices {

    @Autowired
    private WebDriver driver;

    @Autowired
    private WebDriverWait wait;

    @Autowired
    private EditarVersionadoPage editarVersionadoPage;

    public void writeEditarCrearFechaHora(String fecha, String hora){
        this.editarVersionadoPage.getInputEditarFecha().sendKeys(fecha);
        this.editarVersionadoPage.getInputEditarFecha().sendKeys("Keys.TAB");
        this.editarVersionadoPage.getInputEditarHora().sendKeys(hora);
    }

    public void selectEditarTarifaBase(String tarifa){
        new Select(this.editarVersionadoPage.getSelectEditarTarifaBase()).selectByVisibleText(tarifa);
    }

    public void selectEditarTarifaFactor(String tarifa){
        new Select(this.editarVersionadoPage.getSelectEditarTarifaFactor()).selectByVisibleText(tarifa);
    }

    public void selectEditarMapeo(String mapeo){
        new Select(this.editarVersionadoPage.getSelectEditarMapeos()).selectByVisibleText(mapeo);
    }

    public void selectGrupoTarifario(String grupo){
        new Select(this.editarVersionadoPage.getSelectEditarGrupoTarifario()).selectByVisibleText(grupo);
    }

    public void selectEstado(String estado){
        new Select(this.editarVersionadoPage.getSelectEditarEstado()).selectByVisibleText(estado);
    }

    public void selectTarifaAdicional(String tarifa){
        new Select(this.editarVersionadoPage.getSelectEditarTarifaAdicional()).selectByVisibleText(tarifa);
    }

    public void selectTarifaAccesoria(String tarifa){
        new Select(this.editarVersionadoPage.getSelectEditarTarifaAccesoria()).selectByVisibleText(tarifa);
    }

    public void clickOnButtonPublicar() {
        this.editarVersionadoPage.getButtonEditarPublicar().click();
    }

    public void clickOnButtonCancelar() {
        this.editarVersionadoPage.getButtonEditarCancelar().click();
    }

    public void clickOnButtonConfirmar() {
        this.editarVersionadoPage.getButtonEditarConfirmar().click();
    }

    public String getMensajeEditar(){
        this.wait.until(ExpectedConditions.visibilityOf(this.editarVersionadoPage.getMensajeEditar()));
        return this.editarVersionadoPage.getMensajeEditar().getText();
    }

}
