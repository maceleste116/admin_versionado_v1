package qa.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.util.SortVersionado;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class HomeVersionadoServices {

    @Autowired
    private WebDriver driver;

    @Autowired
    private WebDriverWait wait;

    @Autowired
    private HomeVersionadoPage homeVersionadoPage;

    public void clickOnMenuPrincipal() {
        this.wait.until(ExpectedConditions.visibilityOf(this.homeVersionadoPage.getMenuPrincipal()));
        this.homeVersionadoPage.getMenuPrincipal().click();
    }

    public void clickOnMenuVersionado() {
        this.wait.until(ExpectedConditions.visibilityOf(homeVersionadoPage.getMenuVersionado()));
        this.homeVersionadoPage.getMenuVersionado().click();
    }

    public void clickOnButtonCrear() {
        this.wait.until(ExpectedConditions.visibilityOf(homeVersionadoPage.getButtonCrear()));
        this.homeVersionadoPage.getButtonCrear().click();
    }

    public void clickOnButtonVolver(){
        this.homeVersionadoPage.getButtonVolverMenuPrincipal().click();
    }

    public void clickOnTablaMostrarAccion() {
        this.wait.until(ExpectedConditions.visibilityOf(this.homeVersionadoPage.getTablaMostrarAccion()));
        this.homeVersionadoPage.getTablaMostrarAccion().click();
    }

    public void clickOnTablaButtonEditar() {
        this.wait.until(ExpectedConditions.visibilityOf(homeVersionadoPage.getTablaButtonEditar()));
        this.homeVersionadoPage.getTablaButtonEditar().click();
    }

    public void clickOnTablaButtonEliminar() {
        this.wait.until(ExpectedConditions.visibilityOf(homeVersionadoPage.getTablaButtonEliminar()));
        this.homeVersionadoPage.getTablaButtonEliminar().click();
    }



    //TABLA

    public void clickOnIconAsc(){
        this.wait.until(ExpectedConditions.visibilityOf(homeVersionadoPage.getIconSortAsc()));
        homeVersionadoPage.getIconSortAsc().click();
    }

    public void clickOnIconDesc(){
        homeVersionadoPage.getIconSortDesc().click();
    }

    public boolean sortAscTableVersionado() {

        SortVersionado sortVersionado = new SortVersionado();

        List<String> allItems = homeVersionadoPage.getMostrarTableHome().findElements(By.className("item"))
                .stream()
                .map(WebElement::getText)
                .map(String::trim)
                .collect(Collectors.toList()); //obtener todos los ítems de la tabla sin encabezado

        //System.out.println(allItems); print todos los items
        //System.out.println(allItems.get(0)); // print primer elemento
        //System.out.println(allItems.size()); // print tamaño de la lista

        List<String> firstColumn = new ArrayList<String>(); //Obtener la primera columna
        List<String> firstColumnAsc = new ArrayList<String>();

        int multiplo = 9; // obtener todos los items de la primera columna
        for(int i=0; i<allItems.size(); i++){
            if (i%multiplo == 0 ){
                firstColumn.add(allItems.get(i));
                firstColumnAsc.add(allItems.get(i));
            }
        }
        System.out.println("Antes de llamar al método:" + firstColumn);
        System.out.println("Antes de llamar al método:" + firstColumnAsc);
        firstColumnAsc = sortVersionado.sortAscVersionado(firstColumnAsc);
        System.out.println("Después de llamar al método:" + firstColumn);
        System.out.println("Después de llamar al método:" + firstColumnAsc);


        int count = 0;
        for(int i=0; i<firstColumn.size(); i++) {
            if (firstColumn.get(i).equals(firstColumnAsc.get(i))) {
                count++;

            }
        }
        if (count == firstColumn.size()) {
            return true;

        }else {
            return false;

        }
    }

    public boolean sortDescTableVersionado() {

        SortVersionado sortVersionado = new SortVersionado();

        List<String> allItems = homeVersionadoPage.getMostrarTableHome().findElements(By.className("item"))   // get table headers
                .stream()
                .map(WebElement::getText)
                .map(String::trim)
                .collect(Collectors.toList());

        List<String> firstColumn = new ArrayList<String>();
        List<String> firstColumnDesc = new ArrayList<>();
        int multiplo = 9;
        for(int i=0; i<allItems.size(); i++){
            if (i%multiplo == 0 ){
                firstColumn.add(allItems.get(i));
                firstColumnDesc.add(allItems.get(i));
            }
        }

        firstColumnDesc = sortVersionado.sortDescVersionado(firstColumnDesc);

        int count = 0;
        for(int i=0; i<firstColumn.size(); i++) {
            if (firstColumn.get(i).equals(firstColumnDesc.get(i))) {
                count++;

            }
        }
        if (count == firstColumn.size()) {
            return true;

        }else {
            return false;

        }
    }


}
