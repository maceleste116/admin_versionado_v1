package qa.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CrearVersionadoServices {

    @Autowired
    private WebDriver driver;

    @Autowired
    private WebDriverWait wait;

    @Autowired
    private CrearVersionadoPage crearVersionadoPage;

    public void writeInputCrearVersion(String version){
        this.wait.until(ExpectedConditions.visibilityOf(this.crearVersionadoPage.getInputCrearVersion()));
        this.crearVersionadoPage.getInputCrearVersion().sendKeys(version);
    }

    public void writeInputCrearFechaHora(String fecha, String hora){
        this.crearVersionadoPage.getInputCrearFecha().sendKeys(fecha);
        this.crearVersionadoPage.getInputCrearFecha().sendKeys("Keys.TAB");
        this.crearVersionadoPage.getInputCrearHora().sendKeys(hora);
    }

    public void selectTarifaBase(String tarifa){
        new Select(this.crearVersionadoPage.getSelectTarifaBase()).selectByVisibleText(tarifa);
    }

    public void selectTarifaFactor(String tarifa){
        new Select(this.crearVersionadoPage.getSelectTarifaFactor()).selectByVisibleText(tarifa);
    }

    public void selectMapeo(String mapeo){
        new Select(this.crearVersionadoPage.getSelectMapeos()).selectByVisibleText(mapeo);
    }

    public void selectGrupoTarifario(String grupo){
        new Select(this.crearVersionadoPage.getSelectGrupoTarifario()).selectByVisibleText(grupo);
    }

    public void selectEstado(String estado){
        new Select(this.crearVersionadoPage.getSelectEstado()).selectByVisibleText(estado);
    }

    public void selectTarifaAdicional(String tarifa){
        new Select(this.crearVersionadoPage.getSelectTarifaAdicional()).selectByVisibleText(tarifa);
    }

    public void selectTarifaAccesoria(String tarifa){
        new Select(this.crearVersionadoPage.getSelectTarifaAccesoria()).selectByVisibleText(tarifa);
    }

    public void clickOnButtonPublicar() {
        this.crearVersionadoPage.getButtonPublicar().click();
    }

    public void clickOnButtonCancelar() {
        this.crearVersionadoPage.getButtonCancelar().click();
    }

    public void clickOnButtonConfirmar() {
        this.crearVersionadoPage.getButtonConfirmar().click();
    }

    public String getMensajeCrear(){
        this.wait.until(ExpectedConditions.visibilityOf(this.crearVersionadoPage.getMensajeCrear()));
        return this.crearVersionadoPage.getMensajeCrear().getText();
    }


}
