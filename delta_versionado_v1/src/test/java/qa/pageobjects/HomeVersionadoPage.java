package qa.pageobjects;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Getter
public class HomeVersionadoPage extends PageBase {

    @Autowired
    public HomeVersionadoPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id="nabvar-burguer-menu-boton-desplegar")
    private WebElement menuPrincipal;

    @FindBy(id="nabvar-burguer-menu-boton-versionados")
    private WebElement menuVersionado;

    @FindBy(id="versionados-tabla-boton-mostrar-acciones-0")
    private WebElement tablaMostrarAccion;

    @FindBy(id="versionados-boton-crear")
    private WebElement buttonCrear;

    @FindBy(id="versionados-boton-volver")
    private WebElement buttonVolverMenuPrincipal;

    @FindBy(id="versionados-tabla-boton-editar-0")
    private WebElement tablaButtonEditar;

    @FindBy(id="versionados-tabla-boton-eliminar-0")
    private WebElement tablaButtonEliminar;



    //TABLA

    @FindBy(id="versionados-tabla")
    private WebElement mostrarTableHome;

    @FindBy(id="versionados-tabla-icono-orden-version")
    private WebElement iconSortAsc;

    @FindBy(id="versionados-tabla-icono-orden-version")
    private WebElement iconSortDesc;







}
