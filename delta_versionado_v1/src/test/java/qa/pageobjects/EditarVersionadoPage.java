package qa.pageobjects;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Getter
public class EditarVersionadoPage extends PageBase {

    @Autowired
    public EditarVersionadoPage(WebDriver driver){
        super(driver);
    }

    @FindBy(id="editar-versionado-date-picker-fechaDesde-fecha")
    private WebElement inputEditarFecha;

    @FindBy(id="editar-versionado-date-picker-fechaDesde-hora")
    private WebElement inputEditarHora;

    @FindBy(id="editar-versionado-select-idTarifaBase")
    private WebElement selectEditarTarifaBase;

    @FindBy(id="editar-versionado-select-idTarifaFactor")
    private WebElement selectEditarTarifaFactor;

    @FindBy(id="editar-versionado-select-mapeoVersionId")
    private WebElement selectEditarMapeos;

    @FindBy(id="editar-versionado-select-grupoTarifarioId")
    private WebElement selectEditarGrupoTarifario;

    @FindBy(id="editar-versionado-select-estadoId")
    private WebElement selectEditarEstado;

    @FindBy(id="editar-versionado-select-idTarifaAdicionales")
    private WebElement selectEditarTarifaAdicional;

    @FindBy(id="editar-versionado-select-idTarifaAccesorios")
    private WebElement selectEditarTarifaAccesoria;

    @FindBy(id="editar-versionado-boton-Publicar")
    private WebElement buttonEditarPublicar;

    @FindBy(id="editar-versionado-boton-Cancelar")
    private WebElement buttonEditarCancelar;

    @FindBy(id="editar-versionado-boton-Confirmar")
    private WebElement buttonEditarConfirmar;

    @FindBy(id="editar-versionado-modal-mensaje")
    private WebElement mensajeEditar;



}
