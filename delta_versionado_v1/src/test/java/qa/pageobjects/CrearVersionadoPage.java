package qa.pageobjects;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Getter
public class CrearVersionadoPage extends PageBase {

    @Autowired
    public CrearVersionadoPage(WebDriver driver){
        super(driver);
    }

    @FindBy(id="crear-versionado-input-version")
    private WebElement inputCrearVersion;

    @FindBy(id="crear-versionado-date-picker-fechaDesde-fecha")
    private WebElement inputCrearFecha;

    @FindBy(id="crear-versionado-date-picker-fechaDesde-hora")
    private WebElement inputCrearHora;

    @FindBy(id="crear-versionado-select-idTarifaBase")
    private WebElement selectTarifaBase;

    @FindBy(id="crear-versionado-select-idTarifaFactor")
    private WebElement selectTarifaFactor;

    @FindBy(id="crear-versionado-select-mapeoVersionId")
    private WebElement selectMapeos;

    @FindBy(id="crear-versionado-select-grupoTarifarioId")
    private WebElement selectGrupoTarifario;

    @FindBy(id="crear-versionado-select-estadoId")
    private WebElement selectEstado;

    @FindBy(id="crear-versionado-select-idTarifaAdicionales")
    private WebElement selectTarifaAdicional;

    @FindBy(id="crear-versionado-select-idTarifaAccesorios")
    private WebElement selectTarifaAccesoria;

    @FindBy(id="crear-versionado-boton-Publicar")
    private WebElement buttonPublicar;

    @FindBy(id="crear-versionado-boton-Cancelar")
    private WebElement buttonCancelar;

    @FindBy(id="crear-versionado-boton-Confirmar")
    private WebElement buttonConfirmar;

    @FindBy(id="crear-versionado-modal-mensaje")
    private WebElement mensajeCrear;



}
