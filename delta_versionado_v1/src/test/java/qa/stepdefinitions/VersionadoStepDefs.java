package qa.stepdefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import qa.conf.DriverConfig;
import qa.tasks.*;

@CucumberContextConfiguration
@ContextConfiguration(classes = {DriverConfig.class})
public class VersionadoStepDefs {

    @Autowired
    private NavigateTo navigateTo;

    @Autowired
    private MenuVersionado menuVersionado;

    @Autowired
    private CrearVersionado crearVersionado;

    @Autowired
    private ValidarCrearVersionado validarCrearVersionado;

    @Autowired
    private EditarVersionado editarVersionado;

    @Autowired
    private ValidarEditarVersionado validarEditarVersionado;

    @Autowired
    private EliminarVersionado eliminarVersionado;

    @Autowired
    private ValidarEliminarVersionado validarEliminarVersionado;


    @Before
    public void goHomePage() throws InterruptedException {
        navigateTo.homePage();
        Thread.sleep(1000);
    }

    @Given("^actuario quiere ir a versionados$")
    public void actuario_quiere_ir_a_versionados() throws Throwable {
        menuVersionado.irMenuVersionado();
        Thread.sleep(1000);
    }

    //CREAR VERSIONADO CON DATOS REQUERIDOS
    @When("^actuario quiere crear versionado con datos requeridos$")
    public void actuario_quiere_crear_versionado_con_datos_requeridos() throws Throwable {
        crearVersionado.withInfoRequired();
        Thread.sleep(1000);
    }

    @Then("^actuario tiene versionado creado con datos requeridos$")
    public void actuario_tiene_versionado_creado_con_datos_requeridos() throws Throwable {
        Assert.assertTrue(validarCrearVersionado.validarVersionadoWithInfoDefault());
    }

    //CREAR VERSIONADO SIN DATOS REQUERIDOS
    @When("^actuario quiere crear versionado sin datos requeridos$")
    public void actuario_quiere_crear_versionado_sin_datos_requeridos() throws Throwable {
        crearVersionado.withOutInfoRequired();
        Thread.sleep(1000);
    }

    @Then("^actuario no tiene versionado creado sin datos requeridos$")
    public void actuario_no_tiene_versionado_creado_sin_datos_requeridos() throws Throwable {
        Assert.assertTrue(validarCrearVersionado.validarVersionadoWithOutInfoDefault());
    }

    //CREAR VERSIONADO CON VERSIÓN EXISTENTE
    @When("^actuario quiere crear versionado con version existente$")
    public void actuario_quiere_crear_versionado_con_version_existente() throws Throwable {
        crearVersionado.withVersionExistente();
        Thread.sleep(1000);
    }

    @Then("^actuario no tiene versionado creado con version existente$")
    public void actuario_no_tiene_versionado_creado_con_version_existente() throws Throwable {
        Assert.assertTrue(validarCrearVersionado.validarVersionadoWithVersionExistente());
    }

    //EDITAR VERSIONADO CON ESTADO EN EDICIÓN
    @When("^actuario quiere editar versionado existente en estado edicion$")
    public void actuario_quiere_editar_versionado_existente_en_estado_edicion() throws Throwable {
        editarVersionado.withEstadoEdición();
        Thread.sleep(1000);
    }

    @Then("^actuario tiene versionado editado en estado edicion$")
    public void actuario_tiene_versionado_editado_en_estado_edicion() throws Throwable {
        Assert.assertTrue(validarEditarVersionado.validarVersionadoEditarWithEstadoEdicion());
    }

    //EDITAR VERSIONADO CON ESTADO PUBLICADA
    @When("^actuario quiere editar versionado existente en estado publicada$")
    public void actuario_quiere_editar_versionado_existente_en_estado_publicada() throws Throwable {
        editarVersionado.withEstadoPublicada();
        Thread.sleep(1000);
    }

    @Then("^actuario no tiene versionado editado en estado publicada$")
    public void actuario_no_tiene_versionado_editado_en_estado_publicada() throws Throwable {
        Assert.assertFalse(validarEditarVersionado.validarVersionadoEditarWithEstadoPublicada());
    }

    //EDITAR VERSIONADO CON ESTADO INACTIVA
    @When("^actuario quiere editar versionado existente en estado inactiva$")
    public void actuario_quiere_editar_versionado_existente_en_estado_inactiva() throws Throwable {
        editarVersionado.withEstadoInactiva();
        Thread.sleep(1000);
    }

    @Then("^actuario no tiene versionado editado en estado inactiva$")
    public void actuario_no_tiene_versionado_editado_en_estado_inactiva() throws Throwable {
        Assert.assertFalse(validarEditarVersionado.validarVersionadoEditarWithEstadoInactiva());
    }

    // PUBLICAR CON ESTADO EDICIÓN
    @When("^actuario quiere publicar versionado estado edicion$")
    public void actuario_quiere_publicar_versionado_estado_edicion() throws Throwable {
        editarVersionado.publicarEstadoEdiciónPublicada();
        Thread.sleep(1000);

    }

    @Then("^actuario tiene versionado publicado estado edición$")
    public void actuario_tiene_versionado_publicado_estado_edicin() throws Throwable {
        Assert.assertTrue(validarEditarVersionado.validarPublicarEdición());
    }

    // PUBLICAR CON ESTADO PUBLICADA
    @When("^actuario quiere publicar versionado estado publicada$")
    public void actuario_quiere_publicar_versionado_estado_publicada() throws Throwable {
        editarVersionado.publicarEstadoEdiciónPublicada();

    }

    @Then("^actuario no tiene versionado publicado estado publicada$")
    public void actuario_no_tiene_versionado_publicado_estado_publicada() throws Throwable {
        Assert.assertTrue(validarEditarVersionado.validarPublicarPublicada());
    }

    // PUBLICAR CON ESTADO INACTIVA
    @When("^actuario quiere publicar versionado estado inactiva$")
    public void actuario_quiere_publicar_versionado_estado_inactiva() throws Throwable {
        editarVersionado.publicarEstadoEdiciónPublicada();
    }

    @Then("^actuario no tiene versionado publicado estado inactiva$")
    public void actuario_no_tiene_versionado_publicado_estado_inactiva() throws Throwable {
        Assert.assertTrue(validarEditarVersionado.validarPublicarInactiva());
    }

    // PUBLICAR CON ESTADO EDICIÓN CON FECHA ANTERIOR
    @When("^actuario quiere publicar versionado estado edición con fecha anterior$")
    public void actuario_quiere_publicar_versionado_estado_edicin_con_fecha_anterior() throws Throwable {
       editarVersionado.publicarEstadoEdiciónFechaAnterior();
    }

    @Then("^actuario no tiene versionado publicado estado edición con fecha anterior$")
    public void actuario_no_tiene_versionado_publicado_estado_edicin_con_fecha_anterior() throws Throwable {
        Assert.assertTrue(validarEditarVersionado.validarPublicarInactiva());
    }

    //ELIMINAR VERSIONADO CON ESTADO EDICIÓN
    @When("^actuario quiere eliminar versionado estado edicion$")
    public void actuario_quiere_eliminar_versionado_estado_edicion() throws Throwable {
        eliminarVersionado.withEstadoEdiciónPublicadaInactiva();
        Thread.sleep(1000);
    }

    @Then("^actuario tiene versionado eliminado estado edición$")
    public void actuario_tiene_versionado_eliminado_estado_edicin() throws Throwable {
        Assert.assertTrue(validarEliminarVersionado.validarVersionadoEliminarWithEstadoEdicion());
    }

    //ELIMINAR VERSIONADO CON ESTADO PUBLICADA
    @When("^actuario quiere eliminar versionado estado publicada$")
    public void actuario_quiere_eliminar_versionado_estado_publicada() throws Throwable {
        eliminarVersionado.withEstadoEdiciónPublicadaInactiva();
        Thread.sleep(1000);
    }

    @Then("^actuario no tiene versionado eliminado estado publicada$")
    public void actuario_no_tiene_versionado_eliminado_estado_publicada() throws Throwable {
        Assert.assertTrue(validarEliminarVersionado.validarVersionadoEliminarWithEstadoPublicada());
    }


    //ELIMINAR VERSIONADO CON ESTADO INACTIVA
    @When("^actuario quiere eliminar versionado estado inactiva$")
    public void actuario_quiere_eliminar_versionado_estado_inactiva() throws Throwable {
        eliminarVersionado.withEstadoEdiciónPublicadaInactiva();
        Thread.sleep(1000);
    }

    @Then("^actuario no tiene versionado eliminado estado inactiva$")
    public void actuario_no_tiene_versionado_eliminado_estado_inactiva() throws Throwable {
        Assert.assertTrue(validarEliminarVersionado.validarVersionadoEliminarWithEstadoInactiva());
    }

    //LISTAR VERSIONADO ASCENDENTE
    @When("^actuario quiere ordenar ascendente versionado existente$")
    public void actuario_quiere_ordenar_ascendente_versionado_existente() throws Throwable {
        throw new PendingException();
    }

    @Then("^actuario tiene ordenado ascendente versionado existente$")
    public void actuario_tiene_ordenado_ascendente_versionado_existente() throws Throwable {
        throw new PendingException();
    }

    //LISTAR VERSIONADO DESCENDENTE
    @When("^actuario quiere ordenar descendente versionado existente$")
    public void actuario_quiere_ordenar_descendente_versionado_existente() throws Throwable {
        throw new PendingException();
    }


    @Then("^actuario tiene ordenado descendente versionado existente$")
    public void actuario_tiene_ordenado_descendente_versionado_existente() throws Throwable {
        throw new PendingException();
    }


}
