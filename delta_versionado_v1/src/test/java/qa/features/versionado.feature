Feature: Administrar Versionado Grupo Tarifario

  Background: Ir al menu Versionados
    Given actuario quiere ir a versionados

  Scenario: Crear versionado con datos requeridos
    When actuario quiere crear versionado con datos requeridos
    Then actuario tiene versionado creado con datos requeridos

  Scenario: Crear versionado sin datos requeridos
    When actuario quiere crear versionado sin datos requeridos
    Then actuario no tiene versionado creado sin datos requeridos

  Scenario: Crear versionado con version existente
    When actuario quiere crear versionado con version existente
    Then actuario no tiene versionado creado con version existente

  Scenario: Editar versionado existente en estado edicion
    When actuario quiere editar versionado existente en estado edicion
    Then actuario tiene versionado editado en estado edicion

  Scenario: Editar versionado existente en estado publicada
    When actuario quiere editar versionado existente en estado publicada
    Then actuario no tiene versionado editado en estado publicada

  Scenario: Editar versionado existente en estado inactiva
    When actuario quiere editar versionado existente en estado inactiva
    Then actuario no tiene versionado editado en estado inactiva

  Scenario: Publicar versionado existente estado edición
    When actuario quiere publicar versionado estado edicion
    Then actuario tiene versionado publicado estado edición

  Scenario: Publicar versionado existente estado publicada
    When actuario quiere publicar versionado estado publicada
    Then actuario no tiene versionado publicado estado publicada

  Scenario: Publicar versionado existente estado inactiva
    When actuario quiere publicar versionado estado inactiva
    Then actuario no tiene versionado publicado estado inactiva

  Scenario: Publicar versionado existente estado edición con fecha anterior
    When actuario quiere publicar versionado estado edición con fecha anterior
    Then actuario no tiene versionado publicado estado edición con fecha anterior

  Scenario: Eliminar versionado existente estado edición
    When actuario quiere eliminar versionado estado edicion
    Then actuario tiene versionado eliminado estado edición

  Scenario: Eliminar versionado existente estado publicada
    When actuario quiere eliminar versionado estado publicada
    Then actuario no tiene versionado eliminado estado publicada

  Scenario: Eliminar versionado existente estado inactiva
    When actuario quiere eliminar versionado estado inactiva
    Then actuario no tiene versionado eliminado estado inactiva

  Scenario: Listar ascendente versionado existente
    When actuario quiere ordenar ascendente versionado existente
    Then actuario tiene ordenado ascendente versionado existente

  Scenario: Listar descendente versionado existente
    When actuario quiere ordenar descendente versionado existente
    Then actuario tiene ordenado descendente versionado existente
