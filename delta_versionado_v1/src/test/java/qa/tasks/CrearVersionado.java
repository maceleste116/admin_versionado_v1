package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.CrearVersionadoServices;
import qa.pageobjects.HomeVersionadoServices;

@Component
public class CrearVersionado {

    @Autowired
    private HomeVersionadoServices homeVersionadoServices;

    @Autowired
    private CrearVersionadoServices crearVersionadoServices;

    public void withInfoRequired(){
        homeVersionadoServices.clickOnButtonCrear();
        crearVersionadoServices.writeInputCrearVersion("122047.13");
        crearVersionadoServices.writeInputCrearFechaHora("01/08/2020","17:00");
        crearVersionadoServices.selectTarifaBase("90012");
        crearVersionadoServices.selectTarifaFactor("90012");
        crearVersionadoServices.selectMapeo("1");
        crearVersionadoServices.selectGrupoTarifario("GTESTAGOSTO");
        //crearVersionadoServices.selectEstado("Editando");
        crearVersionadoServices.selectTarifaAdicional("90012");
        crearVersionadoServices.selectTarifaAccesoria("90012");
        crearVersionadoServices.clickOnButtonConfirmar();
    }

    public void withOutInfoRequired(){
        homeVersionadoServices.clickOnButtonCrear();
        crearVersionadoServices.writeInputCrearVersion("");
        crearVersionadoServices.writeInputCrearFechaHora("dd/mm/aaaa","--:--");
        crearVersionadoServices.selectTarifaBase("Seleccione una opción");
        crearVersionadoServices.selectTarifaFactor("Seleccione una opción");
        crearVersionadoServices.selectMapeo("Seleccione una opción");
        crearVersionadoServices.selectGrupoTarifario("Seleccione una opción");
        //crearVersionadoServices.selectEstado("Editando");
        crearVersionadoServices.selectTarifaAdicional("Seleccione una opción");
        crearVersionadoServices.selectTarifaAccesoria("Seleccione una opción");
        crearVersionadoServices.clickOnButtonConfirmar();
    }

    public void withVersionExistente(){
        homeVersionadoServices.clickOnButtonCrear();
        crearVersionadoServices.writeInputCrearVersion("122047.01");
        crearVersionadoServices.writeInputCrearFechaHora("05/08/2020","12:00");
        crearVersionadoServices.selectTarifaBase("90012");
        crearVersionadoServices.selectTarifaFactor("90012");
        crearVersionadoServices.selectMapeo("1");
        crearVersionadoServices.selectGrupoTarifario("GTEST");
        //crearVersionadoServices.selectEstado("Editando");
        crearVersionadoServices.selectTarifaAdicional("90012");
        crearVersionadoServices.selectTarifaAccesoria("90012");
        crearVersionadoServices.clickOnButtonConfirmar();
    }

}
