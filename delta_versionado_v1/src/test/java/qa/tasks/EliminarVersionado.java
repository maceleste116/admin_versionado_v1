package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.EditarVersionadoServices;
import qa.pageobjects.EliminarVersionadoServices;
import qa.pageobjects.HomeVersionadoServices;

@Component
public class EliminarVersionado {

    @Autowired
    private HomeVersionadoServices homeVersionadoServices;

    @Autowired
    private EliminarVersionadoServices eliminarVersionadoServices;

    public void withEstadoEdiciónPublicadaInactiva() throws InterruptedException {
        homeVersionadoServices.clickOnTablaMostrarAccion();
        homeVersionadoServices.clickOnTablaButtonEliminar();
        Thread.sleep(1000);
        eliminarVersionadoServices.clickOnTablaButtonBorrar();

    }


}
