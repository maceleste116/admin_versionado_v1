package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.HomeVersionadoServices;

@Component
public class MenuVersionado {
    @Autowired
    private HomeVersionadoServices homeVersionadoServices;

    public void irMenuVersionado(){
        homeVersionadoServices.clickOnMenuPrincipal();
        homeVersionadoServices.clickOnMenuVersionado();
    }
}
