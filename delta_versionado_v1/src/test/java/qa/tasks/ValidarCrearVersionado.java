package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.CrearVersionadoServices;

@Component
public class ValidarCrearVersionado {

    @Autowired
    private CrearVersionadoServices crearVersionadoServices;

    public boolean validarVersionadoWithInfoDefault(){
        String mensaje = crearVersionadoServices.getMensajeCrear();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("Se creó versionado con éxito");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;
    }

    public boolean validarVersionadoWithOutInfoDefault(){
        String mensaje = crearVersionadoServices.getMensajeCrear();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("Faltan campos");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;

    }

    public boolean validarVersionadoWithVersionExistente(){
        String mensaje = crearVersionadoServices.getMensajeCrear();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("Falló al crear versionado");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;

    }

}
