package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.EditarVersionadoServices;

@Component
public class ValidarEditarVersionado {

    @Autowired
    private EditarVersionadoServices editarVersionadoServices;

    public boolean validarVersionadoEditarWithEstadoEdicion(){
        String mensaje = editarVersionadoServices.getMensajeEditar();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("Se actualizó versionado");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;
    }

    public boolean validarVersionadoEditarWithEstadoPublicada(){
        String mensaje = editarVersionadoServices.getMensajeEditar();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("Solo se puede editar un versionado de grupo tarifario con estado En Edición");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;
    }

    public boolean validarVersionadoEditarWithEstadoInactiva(){
        String mensaje = editarVersionadoServices.getMensajeEditar();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("Solo se puede editar un versionado de grupo tarifario con estado En Edición");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;
    }

    public boolean validarPublicarEdición(){
        String mensaje = editarVersionadoServices.getMensajeEditar();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("El versionado quedó guardado y estará publicado");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;
    }

    public boolean validarPublicarPublicada(){
        String mensaje = editarVersionadoServices.getMensajeEditar();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("El versionado se encuentra en estado publicada");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;
    }

    public boolean validarPublicarInactiva(){
        String mensaje = editarVersionadoServices.getMensajeEditar();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("El versionado se encuentra en estado inactiva");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;
    }

    public boolean validarPublicarFechaAnterior(){
        String mensaje = editarVersionadoServices.getMensajeEditar();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("La fecha desde el cual queda activa la versión debe ser mayor al día de hoy");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;
    }




}
